contents = File.read('./hollister_tops.html')

# src="//anf.scene7.com/is/image/anf/hol_186392_01_model1?$product-ofp-hol-v1$&$category-ofp-hol-v1.1$"

# img_src_pattern = /src="\/\/anf.scene7.com\/is\/image\/anf\/hol_.*\$product-ofp-hol-v1\$\&\$category-ofp-hol-v1.1\$"/i


# <a class="grid-product__name" href="/shop/uk/p/sherpa-lined-graphic-hoodie-10259254?seq=01&amp;ofp=true" data-productid="10259254">Sherpa-Lined Graphic Hoodie</a>
# <span class="product-price-v2__price product-price-v2__price--offer"> £27.50 </span>
#
# '<a class="grid-product__name" href="/shop/uk/p/sherpa-lined-graphic-hoodie-10259254?seq=01&amp;ofp=true" data-productid="10259254">Sherpa-Lined Graphic Hoodie</a>'
#
# <a class="grid-product__name" href="/shop/uk/p/full-zip-hooded-sweater-10116733?categoryId=166329&amp;seq=04&amp;ofp=true" data-productid="10116733">Full-Zip Hooded Sweater</a>
# <span class="product-price-v2__price product-price-v2__price--list"> £39 </span>




img_pattern = /src="\/\/anf.scene7.com\/is\/image\/anf\/hol_\d{6}_\d{2}_/
img_ending = "model1?$product-ofp-hol-v1$&$category-ofp-hol-v1.1$"
http = "http://"
img_matches = contents.scan(img_pattern)
# puts img_matches.length
# puts img_matches[0,10].map {|url| url[7..-1] + img_ending}


name_pattern = /<a class="grid-product__name" href="\/shop\/uk\/p\/.{1,100} data-product.d="\d{1,20}".(.{1,100})<\/a>/
name_matches = contents.scan(name_pattern)

# puts name_matches.length
# puts name_matches[0,10].inspect


html_header = '<html><head><style>div.product {display: inline-block;width: 19%;height: auto;}</style></head>'
html_footer = '</body></html>'


content_html = ''
(0...img_matches.length).each do |i|
  url = http + img_matches[i][7..-1] + img_ending
  name = "#{i}. #{name_matches[i][0][1..-1]}"
  content_html += "<div class='product'><img src='#{url}'><p>#{name}</p></div>\n"
end

html = html_header + content_html + html_footer
File.write("./hollister_tops_all.html", html)


# (0...img_matches.length).step(48).each do |i|
#
#   content_html = ''
#
#   i.upto(i + 47).each do |j|
#     break if j >= img_matches.length
#     url = http + img_matches[j][7..-1] + img_ending
#     name = "#{j}. #{name_matches[j][0][1..-1]}"
#
#     content_html += "<div class='product'><img src='#{url}'><p>#{name}</p></div>\n"
#
#   end
#
#
#   html = html_header + content_html + html_footer
#   File.write("./hollister_tops_page#{i / 48}.html", html)
#
# end






# products = []
#
# (0...img_matches.length).each do |i|
#   products << {img_url: http + img_matches[i][7..-1] + img_ending , name: "#{i}. #{name_matches[i][0][1..-1]}"}
# end
#
#
# products.step(48).each do |product_hash|
#   #puts "#{product_hash[:img_url]}\n#{product_hash[:name]}\n"
#   url = product_hash[:img_url]
#   name = product_hash[:name]
#   # html = '<div class="product"><img src="#{}"></div>'
#   html = "<div class='product'><img src='#{url}'><p>#{name}</p></div>\n"
#
#   puts html
# end






# price_pattern = /<span class="product-price-v2__price product-price-v2__price--list">\s{0,10}\&pound;(\d{1,4})\s{0,10}<\/span>/
# price_matches = contents.scan(price_pattern)
#
# puts price_matches.length
# puts price_matches[1].inspect
